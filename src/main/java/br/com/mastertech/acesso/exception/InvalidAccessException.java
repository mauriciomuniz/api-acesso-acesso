package br.com.mastertech.acesso.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Acesso inválido.")
public class InvalidAccessException extends RuntimeException{
}
