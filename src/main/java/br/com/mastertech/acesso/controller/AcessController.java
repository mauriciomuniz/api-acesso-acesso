package br.com.mastertech.acesso.controller;

import br.com.mastertech.acesso.DTO.AccessDTO;
import br.com.mastertech.acesso.service.AcessService;
import javassist.NotFoundException;
import org.apache.kafka.common.KafkaException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/acesso")
public class AcessController {

    @Autowired
    public AcessService acessService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public AccessDTO criarAcessoNoSistema(@RequestBody @Valid AccessDTO acess) {
        return acessService.cadastrar(acess);
    }

    @DeleteMapping("/{clienteid}/{portaid}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Transactional
    public void deletarAcessoAoSistema(@PathVariable(name = "clienteid") int clienteId, @PathVariable(name = "portaid") int portaId) {
        acessService.deletarAcesso(clienteId, portaId);
    }

    @GetMapping("/{clienteid}/{portaid}")
    @ResponseStatus(HttpStatus.OK)
    public AccessDTO consultarAcessoNoSistema(@PathVariable(name = "clienteid") int clienteId, @PathVariable(name = "portaid") int portaId) throws NotFoundException {
        try {
            AccessDTO accessDTO = acessService.consultarAcesso(clienteId, portaId);
            return accessDTO;
        } catch (KafkaException e) {
            throw new ResponseStatusException(HttpStatus.REQUEST_TIMEOUT, e.getMessage());
        }

    }
}
