package br.com.mastertech.acesso.repository;

import br.com.mastertech.acesso.model.Access;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AcessRepository extends CrudRepository<Access, Integer> {
    Optional<Access> findByClienteIdAndPortaId(int clienteId, int portaId);

    void deleteByClienteIdAndPortaId(int clienteId, int portaId);
}
