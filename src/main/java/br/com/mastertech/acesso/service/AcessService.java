package br.com.mastertech.acesso.service;

import br.com.mastertech.acesso.DTO.AccessDTO;
import br.com.mastertech.acesso.calls.client.ClienteAcesso;
import br.com.mastertech.acesso.calls.door.PortaAcesso;
import br.com.mastertech.acesso.exception.InvalidAccessException;
import br.com.mastertech.acesso.model.Access;
import br.com.mastertech.acesso.model.Cliente;
import br.com.mastertech.acesso.model.Porta;
import br.com.mastertech.acesso.repository.AcessRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.Optional;
import java.util.Random;

@Service
public class AcessService {

    @Autowired
    public AcessRepository acessRepository;

    @Autowired
    public PortaAcesso portaAcesso;

    @Autowired
    public ClienteAcesso clienteAcesso;

    @Autowired
    private KafkaTemplate<String, Access> producer;

    private static final Duration timeoutTime = Duration.ofSeconds(10);

    private static final String[] TOPICOS = {"spec4-muniz-1", "spec4-muniz-2", "spec4-muniz-3"};


    public AccessDTO cadastrar(AccessDTO accessDTO) {
        try {
            consultarCliente(accessDTO.getCliente_id());
            consultarPorta(accessDTO.getPorta_id());
            Access access = montaEntity(accessDTO);
            acessRepository.save(access);
            return accessDTO;
        } catch (RuntimeException e) {
            throw new InvalidAccessException();
        }
    }

    public Cliente consultarCliente(int clienteId) {
        return clienteAcesso.consultarClientePorId(clienteId);
    }

    public Porta consultarPorta(int portaId) {
        return portaAcesso.consultarPortaPorId(portaId);
    }

    public AccessDTO consultarAcesso(int clienteId, int portaId) throws NotFoundException {
        Random rnd = new Random();

        Optional<Access> acessOptional = acessRepository.findByClienteIdAndPortaId(clienteId, portaId);
        if (!acessOptional.isPresent()) {
            Access acesso = new Access();
            acesso.setPortaId(portaId);
            acesso.setClienteId(clienteId);
            enviarAoKafka(rnd.nextInt(TOPICOS.length), acesso);
            throw new InvalidAccessException();
        }
        AccessDTO accessDTO;
        Access access = acessOptional.get();
        accessDTO = montaDTO(access);
        enviarAoKafka(rnd.nextInt(TOPICOS.length), access);
        return accessDTO;
    }

    public void deletarAcesso(int clienteId, int portaId) {
        acessRepository.deleteByClienteIdAndPortaId(clienteId, portaId);
    }

    public Access montaEntity(AccessDTO accessDTO) {
        Access access = new Access();
        access.setClienteId(accessDTO.getCliente_id());
        access.setPortaId(accessDTO.getPorta_id());
        return access;
    }

    public AccessDTO montaDTO(Access access) {
        AccessDTO accessDTO = new AccessDTO();
        accessDTO.setCliente_id(access.getClienteId());
        accessDTO.setPorta_id(access.getPortaId());
        return accessDTO;
    }

    public void enviarAoKafka(int topicId, Access acesso) {
        producer.setCloseTimeout(timeoutTime);
        producer.send(TOPICOS[topicId], acesso);
    }
}
