package br.com.mastertech.acesso.calls.door;

import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class DoorAcessConfiguration {
    @Bean
    public ErrorDecoder getDoorAcessDecoder() {
        return new DoorAcessDecoder();
    }

    @Bean
    public Feign.Builder builder() {
        FeignDecorators decorators = FeignDecorators.builder()
                .withFallback(new DoorAcessFallback(), RetryableException.class)
                .withFallbackFactory(DoorAcessLoadBalancerFallback::new, RuntimeException.class)
                .build();

        return Resilience4jFeign.builder(decorators);
    }
}
