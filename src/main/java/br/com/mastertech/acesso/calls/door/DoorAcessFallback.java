package br.com.mastertech.acesso.calls.door;

import br.com.mastertech.acesso.model.Porta;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class DoorAcessFallback implements PortaAcesso{

    @Override
    public Porta consultarPortaPorId(int id) {
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "API Porta indisponível");
    }
}
