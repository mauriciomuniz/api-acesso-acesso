package br.com.mastertech.acesso.calls.door;

import br.com.mastertech.acesso.model.Porta;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "PORTA", configuration = DoorAcessConfiguration.class)
public interface PortaAcesso {

    @GetMapping("/porta/{id}")
    Porta consultarPortaPorId(@PathVariable(name = "id") int id);
}