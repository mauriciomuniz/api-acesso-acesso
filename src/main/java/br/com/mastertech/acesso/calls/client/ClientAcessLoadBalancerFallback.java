package br.com.mastertech.acesso.calls.client;

import br.com.mastertech.acesso.model.Cliente;
import com.netflix.client.ClientException;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class ClientAcessLoadBalancerFallback implements ClienteAcesso {

    private Exception exception;

    public ClientAcessLoadBalancerFallback(Exception exception) {
        this.exception = exception;
    }

    @Override
    public Cliente consultarClientePorId(int id) {
        if (exception.getCause() instanceof ClientException) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "API cliente indisponível");
        }
        throw (RuntimeException) exception;
    }
}
