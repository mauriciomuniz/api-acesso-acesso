package br.com.mastertech.acesso.calls.client;

import br.com.mastertech.acesso.model.Cliente;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class ClientAcessFallback implements ClienteAcesso {

    @Override
    public Cliente consultarClientePorId(int id) {
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "API Cliente indisponível");
    }
}
