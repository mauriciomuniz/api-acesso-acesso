package br.com.mastertech.acesso.calls.client;

import br.com.mastertech.acesso.model.Cliente;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "CLIENTE", configuration = ClientAcessConfiguration.class)
public interface ClienteAcesso {

    @GetMapping("/cliente/{id}")
    Cliente consultarClientePorId(@PathVariable(name = "id") int id);
}
