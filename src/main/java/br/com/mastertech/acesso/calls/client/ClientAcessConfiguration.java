package br.com.mastertech.acesso.calls.client;

import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class ClientAcessConfiguration {
    @Bean
    public ErrorDecoder getClientAcessDecoder() {
        return new ClientAcessDecoder();
    }

    @Bean
    public Feign.Builder builder() {
        FeignDecorators decorators = FeignDecorators.builder()
                .withFallback(new ClientAcessFallback(), RetryableException.class)
                .withFallbackFactory(ClientAcessLoadBalancerFallback::new, RuntimeException.class)
                .build();

        return Resilience4jFeign.builder(decorators);
    }
}
