package br.com.mastertech.acesso.calls.door;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Porta não encontrada")
public class DoorNotFoundException extends Exception {
}
