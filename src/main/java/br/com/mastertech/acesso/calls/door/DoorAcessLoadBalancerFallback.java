package br.com.mastertech.acesso.calls.door;

import br.com.mastertech.acesso.model.Porta;
import com.netflix.client.ClientException;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class DoorAcessLoadBalancerFallback implements PortaAcesso{

    private Exception exception;

    public DoorAcessLoadBalancerFallback(Exception exception) {
        this.exception = exception;
    }

    @Override
    public Porta consultarPortaPorId(int id) {
        if (exception.getCause() instanceof ClientException) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "API Porta indisponível");
        }
        throw (RuntimeException) exception;
    }
}
