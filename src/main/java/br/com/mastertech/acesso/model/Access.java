package br.com.mastertech.acesso.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"portaId", "clienteId"}))
@Entity
public class Access {

    @Id
    @GeneratedValue
    private int id;

    @NotNull
    private int portaId;

    @NotNull
    private int clienteId;

    public Access() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPortaId() {
        return portaId;
    }

    public void setPortaId(int portaId) {
        this.portaId = portaId;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }
}
